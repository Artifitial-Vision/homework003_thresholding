import cv2

class ThresHolding:

    @staticmethod
    def binary():
        img = cv2.imread('mustang.jpg')
        img_gs = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        altura, anchura, canales = img.shape

        for al in range(altura):
            for an in range(anchura):
                if img_gs[al, an] < 128:
                    img_gs[al, an] = 0
                else:
                    img_gs[al, an] = 255

        cv2.imshow('BGR', img_gs)
        cv2.waitKey(0)

    @staticmethod
    def binary_inverted():
        img = cv2.imread('mustang.jpg')
        img_gs = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        altura, anchura, canales = img.shape

        for al in range(altura):
            for an in range(anchura):
                if img_gs[al, an] < 128:
                    img_gs[al, an] = 255
                else:
                    img_gs[al, an] = 0

        cv2.imshow('BGR', img_gs)
        cv2.waitKey(0)

    @staticmethod
    def truncate():
        img = cv2.imread('mustang.jpg')
        img_gs = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        altura, anchura, canales = img.shape

        for al in range(altura):
            for an in range(anchura):
                if img_gs[al, an] > 127:
                    img_gs[al, an] = 255

        cv2.imshow('Truncate', img_gs)
        cv2.waitKey(0)

    @staticmethod
    def tozero():
        img = cv2.imread('mustang.jpg')
        img_gs = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        altura, anchura, canales = img.shape

        for al in range(altura):
            for an in range(anchura):
                if img_gs[al, an] < 128:
                    img_gs[al, an] = 0

        cv2.imshow('ToZero', img_gs)
        cv2.waitKey(0)


# ThresHolding.binary()
# ThresHolding.truncate()
ThresHolding.tozero()